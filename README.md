# Stash Repository Filesize Plugin

Provde a REST resource for fetching the filesize of a given repository.
That's it...

## Usage

    curl -u user:pass http://localhost:7990/stash/rest/reposize/latest/projects/$PROJECT/repos/$REPO/
