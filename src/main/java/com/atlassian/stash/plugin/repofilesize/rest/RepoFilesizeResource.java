package com.atlassian.stash.plugin.repofilesize.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.rest.util.ResourcePatterns;
import com.atlassian.stash.rest.util.RestUtils;
import com.atlassian.stash.util.NumberUtils;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(ResourcePatterns.REPOSITORY_URI)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
@AnonymousAllowed
public class RepoFilesizeResource {

    private final RepositoryService repositoryService;

    public RepoFilesizeResource(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @GET
    public Response getFilesize(@Context Repository repository) {
        long size = repositoryService.getSize(repository);
        return Response.ok(ImmutableMap.of(
                "sizeRaw", size,
                "size", NumberUtils.formatSize(size)
        )).build();
    }
}
